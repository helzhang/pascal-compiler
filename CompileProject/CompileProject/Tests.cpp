#include "Tests.h"
#include "Parser.h"
#include <experimental\filesystem>
namespace fs = std::experimental::filesystem;


Tests::Tests()
{
}


Tests::~Tests()
{
}

void Tests::load_test_from_file()
{
	Parser *lex = new Parser("infile.txt", "outfile.txt", false);
	lex->programm();
	delete lex;
}

void Tests::load_test_from_dir(string path)
{
	Parser *parser;
	if (!fs::exists(path))
	{
		cout << "Not exist" << endl;
		return;
	}
	ofstream f;
	f.open("output.txt");
	f.close();
	for (auto & fileInDir : fs::directory_iterator(path))
	{
		f.open("output.txt", ios::app);
		f << "File: " << fileInDir.path() << endl << "-----------------------------------------------------" << endl;
		f.close();
		parser = new Parser(fileInDir.path().string(), "output.txt", true);
		parser->programm();
		delete parser;
		parser = nullptr;
		f.open("output.txt", ios::app);
		f << endl << "-----------------------------------------------------" << endl;
		f.close();
	}
}

void Tests::mainfunc()
{
}
