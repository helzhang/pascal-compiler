#include "Semantic.h"


Semantic::Semantic()
{
}


Semantic::~Semantic()
{
}

void Semantic::createfictscope()
{
	localscope = new Scope(NULL);

	booltype = new EnumType();
	localscope->typetable.push_back(booltype);
	localscope->identtable.insert(pair<string, CIdent*>("boolean", new CIdent(CIdent::TYPES)));
	localscope->identtable["boolean"]->identtype = booltype;

	auto entry = new CIdent(CIdent::CONSTS);
	entry->identtype = booltype;
	((EnumType*)booltype)->constlist.push_back("false");
	localscope->identtable.insert(pair<string, CIdent*>("false", entry));

	entry = new CIdent(CIdent::CONSTS);
	entry->identtype = booltype;
	((EnumType*)booltype)->constlist.push_back("true");
	localscope->identtable.insert(pair<string, CIdent*>("true", entry));

	realtype = new ScalarType(); localscope->typetable.push_back(realtype);
	chartype = new ScalarType(); localscope->typetable.push_back(chartype);
	inttype = new ScalarType(); localscope->typetable.push_back(inttype);
	stringtype = new ScalarType(); localscope->typetable.push_back(stringtype);

	entry = new CConst(MAXINT);
	entry->identtype = inttype;
	localscope->identtable.insert(pair<string, CIdent*>("maxint", entry));

	entry = new CIdent(CIdent::TYPES);
	entry->identtype = inttype;
	localscope->identtable.insert(pair<string, CIdent*>("integer", entry));

	entry = new CIdent(CIdent::TYPES);
	entry->identtype = realtype;
	localscope->identtable.insert(pair<string, CIdent*>("real", entry));

	entry = new CIdent(CIdent::TYPES);
	entry->identtype = chartype;
	localscope->identtable.insert(pair<string, CIdent*>("char", entry));

	entry = new CIdent(CIdent::TYPES);
	entry->identtype = stringtype;
	localscope->identtable.insert(pair<string, CIdent*>("string", entry));
}

void Semantic::deletefictscope()
{
	delete localscope;
	localscope = nullptr;
}

void Semantic::createscope()
{
	auto old = localscope;
	localscope = new Scope(old);
	old = nullptr;
}

void Semantic::deletescope()
{
	auto old = localscope;
	localscope = localscope->enclosingscope;
	delete old;
	old = nullptr;
}

Type * Semantic::gettypefromident(CSymbol *symbol, CLexer* lexer)
{
	Scope* it = localscope;
	while (it != NULL) 
	{
		if (it->identtable.find(symbol->name) != it->identtable.end())
		{
			if (it->identtable[symbol->name]->useclass == CIdent::TYPES)
			{
				return it->identtable[symbol->name]->identtype;
			}
			else
			{
				lexer->error(100);
				return nullptr;
			}
		}
		it = it->enclosingscope;
	}
	neutralization(CIdent::TYPES, symbol->name);
	lexer->error(104);
	return nullptr;
}

void Semantic::addvars(vector<CIdent*> idents, vector<string> names, vector<textposition> tokens, CSymbol * type, CLexer *lexer)
{
	auto t = gettypefromident(type, lexer);
	for (unsigned int i = 0; i < idents.size(); i++)
	{
		if (localscope->identtable.find(names[i]) != localscope->identtable.end())
			lexer->error(101, tokens[i]);
		else
		{
			idents[i]->identtype = t;
			localscope->identtable.insert(pair<string, CIdent *>(names[i], idents[i]));
		}
	}
}

void Semantic::addprog(CSymbol * progident)
{
	auto prog = new CIdent(CIdent::PROGS);
	prog->identtype = nullptr;
	localscope->identtable.insert(pair<string, CIdent*>(progident->name, prog));
}

void Semantic::addtype(string name, textposition token, CSymbol * type, CLexer * lexer)
{
	if (localscope->identtable.find(name) != localscope->identtable.end())
		lexer->error(101, token);
	else
	{
		auto t = gettypefromident(type, lexer);
		auto id = new CIdent(CIdent::TYPES);
		id->identtype = t;
		localscope->identtable.insert(pair<string, CIdent *>(name, id));
		localscope->typetable.push_back(t);
		t = nullptr;
		id = nullptr;
	}
}

void Semantic::addproc(CIdent * ident, string name)
{
	ident->identtype = nullptr;
	localscope->identtable.insert(pair<string, CIdent*>(name, ident));
}

void Semantic::addparam(CIdent * id, string name)
{
	localscope->identtable.insert(pair<string, CIdent*>(name, id));
}

void Semantic::addparam(vector<CIdent*> idents, vector<string> names, vector<textposition> tokens, CSymbol * type, CLexer * lexer, CProcedure * proc, int mettransf)
{
	auto t = gettypefromident(type, lexer);
	for (unsigned int i = 0; i < idents.size(); i++)
	{
		if (localscope->identtable.find(names[i]) != localscope->identtable.end())
			lexer->error(101, tokens[i]);
		else
		{
			idents[i]->identtype = t;
			ProcParam *par = new ProcParam(mettransf);
			par->paramtype = t;
			proc->parameters.push_back(par);
			localscope->identtable.insert(pair<string, CIdent *>(names[i], idents[i]));
		}
	}
}

CIdent * Semantic::getidentfromname(string name)
{
	Scope* it = localscope;
	while (it != NULL)
	{
		if (it->identtable.find(name) != it->identtable.end())
		{
			return it->identtable[name];
		}
		it = it->enclosingscope;
	}
	return nullptr;
}

CIdent * Semantic::getidentfromthisscope(string name)
{
	if (localscope->identtable.find(name) != localscope->identtable.end())
	{
		return localscope->identtable[name];
	}
	return nullptr;
}

Type * Semantic::checklogical(Type * type, CLexer* lexer)
{
	if (type == booltype || type == nullptr) return type;
	else
	{
		lexer->error(135);
		return nullptr;
	}
}

Type * Semantic::test_mult(Type * type1, Type * type2, unsigned op, CLexer * lexer)
{
	if (type1 == nullptr || type2 == nullptr) return nullptr;
	switch (op)
	{
	case star:
		if (type1 != inttype && type1 != realtype || type2 != inttype && type2 != realtype)
		{
			lexer->error(213);
			return nullptr;
		}
		if (type1 == realtype || type2 == realtype)
			return realtype;
		else
			return inttype;
		break;
	case slash:
		if (type1 != inttype && type1 != realtype || type2 != inttype && type2 != realtype)
		{
			lexer->error(214);
			return nullptr;
		}
		return realtype;
		break;
	case andsy:
		if (type1 != booltype || type2 != booltype)
		{
			lexer->error(210);
			return nullptr;
		}
		return booltype;
		break;
	case divsy:
	case modsy:
		if (type1 != inttype || type2 != inttype)
		{
			lexer->error(212);
			return nullptr;
		}
		return inttype;
		break;
	}
	return nullptr;
}

Type * Semantic::test_add(Type * type1, Type * type2, unsigned op, CLexer * lexer)
{
	if (type1 == nullptr || type2 == nullptr) return nullptr;
	switch (op)
	{
	case symbols::plus:
		if (type1 == booltype || type2 == booltype)
		{
			lexer->error(211);
			return nullptr;
		}
		if ((type1 == chartype || type1 == stringtype) && (type2 == chartype || type2 == stringtype))
			return stringtype;
		if (type1 == inttype && type2 == inttype)
			return inttype;
		if (type1 == inttype && type2 == realtype ||
			type1 == realtype && type2 == inttype ||
			type1 == realtype && type2 == realtype)
			return realtype;
		lexer->error(189);
		break;
	case symbols::minus:
		if (type1 != inttype && type1 != realtype || type2 != inttype && type2 != realtype)
		{
			lexer->error(211);
			return nullptr;
		}
		if (type1 == inttype && type2 == inttype)
			return inttype;
		if (type1 == inttype && type2 == realtype ||
			type1 == realtype && type2 == inttype ||
			type1 == realtype && type2 == realtype)
			return realtype;
		break;
	case orsy:
		if (type1 != booltype || type2 != booltype)
		{
			lexer->error(210);
			return nullptr;
		}
		return booltype;
		break;
	}
	return nullptr;
}

void Semantic::right_sign(Type * type, CLexer * lexer)
{
	if (type != inttype && type != realtype)
	{
		lexer->error(184);
	}
}

Type * Semantic::comparing(Type * type1, Type * type2, unsigned op, CLexer * lexer)
{
	if (type1 == type2) return booltype;
	if (type1 == realtype && type2 == inttype ||
		type1 == inttype && type2 == realtype)
		return booltype;
	if (type1 == chartype && type2 == stringtype ||
		type1 == stringtype && type2 == chartype)
		return booltype;
	lexer->error(186);
	return nullptr;
}

bool Semantic::compatible_assign(Type * vartype, Type * exprtype)
{
	if (vartype == nullptr || exprtype == nullptr) return true;
	if (vartype == exprtype) return true;
	if (vartype == realtype && exprtype == inttype) return true;
	if (vartype == stringtype && exprtype == chartype) return true;
	return false;
}

Type * Semantic::checkcase(Type * exprtype, CLexer * lexer)
{
	if (exprtype == booltype || exprtype == inttype || exprtype == chartype)
		return exprtype;
	else
	{
		lexer->error(309);
		return nullptr;
	}
}

void Semantic::neutralization(CIdent::UseClass cl, string name)
{
	auto id = new CIdent(cl);
	id->identtype = nullptr;
	localscope->identtable.insert(pair<string, CIdent*>(name, id));
	id = nullptr;
}

void Semantic::checkprocparams(CProcedure * proc, vector<Type*> types, vector<textposition> tokens, CLexer* lexer)
{
	if (proc->parameters.size() != types.size())
	{
		lexer->error(198);
		return;
	}
	for (unsigned i = 0; i < types.size(); i++)
	{
		if (!compatible_assign(proc->parameters[i]->paramtype, types[i]))
		{
			lexer->error(199, tokens[i]);
		}
	}
}
