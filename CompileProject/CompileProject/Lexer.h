#pragma once
#include <fstream>
#include <iostream>
#include <string>
#include <vector>
#include "SymConsts.h"
#include "CSymbol.h"
using namespace std;

#define ENDSTR 1000

struct errorList
{
	textposition errorposition;
	unsigned errorcode;
};

class CLexer
{
private:
	char ch;
	textposition positionnow;
	//ifstream ifstr;
	ofstream ofstr;
	string line;
	vector<errorList> ErrList;
public:
	ifstream ifstr;
	const short ERRMAX = 99;
	short errorcount=0;
	CLexer(string ifile, string ofile, bool isApp);
	~CLexer();
	void nextch();
	void error(unsigned errorcode, textposition position);
	void error(unsigned errorcode);
	void printError(unsigned errorcode, textposition position);
private:
	void PrintErrors(vector<errorList> errline, unsigned num);
	vector<errorList> FindErrors(unsigned linenum);


public:
	CSymbol* nextsym();
	const int MAX_LEN_IDENT = 63;
private:
	CSymbol* symbol;
	void OneLineComm();
	void MultiLineComm(bool isParstar);
	void SymbolConst();
	void SymErr();
	void NumConst();
	void Words();
};

