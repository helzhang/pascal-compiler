#include "Parser.h"
#include "Tests.h"
using namespace std;

int main()
{
	Tests test;
	int i; string dir;
	cout << "1 - from file" << endl << "2 - from directory" << endl;
	cin >> i;
	if (i == 2)
	{
		cout << "Directory path" << endl;
		cin >> dir;
		test.load_test_from_dir(dir);
	}
	else if (i == 1)
	{
		test.load_test_from_file();
	}
	return 0;
}