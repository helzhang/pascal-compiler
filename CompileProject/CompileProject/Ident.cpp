#include "Ident.h"


CIdent::CIdent(UseClass clas)
{
	useclass = clas;
}


CIdent::~CIdent()
{
}

CConst::CConst(int val) : CIdent(UseClass::CONSTS)
{
	intval = val;
}

CConst::~CConst()
{
}

Scope::Scope(Scope *localscope)
{
	enclosingscope = localscope;
}

Scope::~Scope()
{
	for (auto i=identtable.begin(); i != identtable.end(); i++)
		delete i->second;
	//for (int i=0; i < typetable.size(); i++)
	//	delete typetable[i];
}

CProcedure::CProcedure() :CIdent(UseClass::PROCS)
{
	forward = false;
}

CProcedure::~CProcedure()
{
	for (int i = 0; i < parameters.size(); i++)
		delete parameters[i];
}

ProcParam::ProcParam(int met)
{
	mettransf = met;
}

ProcParam::~ProcParam()
{
}
