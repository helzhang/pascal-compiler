#pragma once
enum symbols {
	star = 21,         /* * */
	slash = 60,        /* / */
	equal = 16,        /* = */
	comma = 20,        /* , */
	semicolon = 14,    /* ; */
	colon = 5,         /* : */
	point = 61,        /* . */
	arrow = 62,        /* ^ */
	leftpar = 9,       /* ( */
	rightpar = 4,      /* ) */
	lbracket = 11,     /* [ */
	rbracket = 12,     /* ] */
	flpar = 63,        /* { */
	frpar = 64,        /* } */
	later = 65,        /* < */
	greater = 66,      /* > */
	laterequal = 67,   /* <= */
	greaterequal = 68, /* => */
	latergreater = 69, /* <> */
	plus = 70,         /* + */
	minus = 71,        /* - */
	lcomment = 72,     /* (* */
	rcomment = 73,     /* )* */
	assign = 51,       /* := */
	twopoints = 74,    /* .. */

	ident = 2,         /* identifier */
	floatc = 82,       /* real constant */
	intc = 15,         /* integer constant */
	charc = 83,        /* character constant */
	stringc = 85,
	dosy = 54,
	ifsy = 56,
	insy = 22,
	ofsy = 8,
	orsy = 23,
	tosy = 55,
	andsy = 24,
	divsy = 25,
	endsy = 13,
	forsy = 26,
	modsy = 27,
	nilsy = 28,
	notsy = 29,
	setsy = 29,
	varsy = 30,
	casesy = 31,
	elsesy = 32,
	filesy = 57,
	gotosy = 33,
	thensy = 52,
	typesy = 34,
	withsy = 37,
	arraysy = 38,
	beginsy = 17,
	constsy = 39,
	labelsy = 40,
	untilsy = 53,
	whilesy = 41,
	downtosy = 55,
	packedsy = 42,
	recordsy = 43,
	repeatsy = 44,
	programsy = 3,
	functionsy = 77,
	proceduresy = 80
};