#include "Parser.h"


Parser::Parser(string ifile, string ofile, bool isApp)
{
	lexer = new CLexer(ifile, ofile, isApp);
	symbol = lexer->nextsym();
	pn = new ParserNeutralization();
	sem = new Semantic();
}


Parser::~Parser()
{
	delete lexer;
	lexer = nullptr;
	delete pn;
	pn = nullptr;
	delete sem;
	sem = nullptr;
}

void Parser::accept(unsigned symbolexpected)
{
	if (symbol->symbol == symbolexpected)
		symbol = lexer->nextsym();
	else {
		lexer->error(symbolexpected, symbol->token);
		if (symbol->symbol == 0) lexer->printError(symbolexpected, symbol->token);
	}
}

void Parser::programm()
{
	sem->createfictscope();
	sem->createscope();
	if (symbol->symbol != programsy)
	{
		lexer->error(3);
		pn->skipto(pn->begpart, lexer, symbol);
	}
	else {
		accept(programsy);
		if (symbol->symbol == ident) sem->addprog(symbol);
		accept(ident);
		if (symbol->symbol == leftpar)
		{
			accept(leftpar);
			accept(ident);
			while (symbol->symbol == comma)
			{
				symbol = lexer->nextsym();
				accept(ident);
			}
			accept(rightpar);
			accept(semicolon);
		}
		else { accept(semicolon); }
	}
	block(pn->start);
	accept(point);
	sem->deletescope();
	sem->deletefictscope();
}

void Parser::block(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->begpart, 18);
	if(pn->belong(symbol->symbol, pn->begpart))
	{
		typepart(pn->set_disjunct(pn->st_varpart, followers));
		varpart(pn->set_disjunct(pn->st_procfuncpart, followers));
		procpart(pn->set_disjunct(pn->st_procfuncpart, followers));
		statementpart();
		pn->forbiddenchar(lexer, symbol, followers);
	}
}

void Parser::typepart(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->begpart, 18);
	if (symbol->symbol == typesy)
	{
		accept(typesy);
		do 
		{
			typedefinition(pn->set_disjunct(pn->after_var, followers));
			accept(semicolon);
		} while (!pn->belong(symbol->symbol, followers));
		pn->forbiddenchar(lexer, symbol, followers);
	}
}

void Parser::typedefinition(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	string name; textposition token;
	pn->skip_if_bad(lexer, symbol, followers, pn->id_starters, 2);
	if (symbol->symbol == ident)
	{
		name = symbol->name;
		token = symbol->token;
		accept(ident);
		accept(symbols::equal);
		if (symbol->symbol == ident) sem->addtype(name, token, symbol, lexer);
		else
		{
			pn->skipto(pn->id_starters, followers, lexer, symbol);
			if (symbol->symbol == ident) sem->addtype(name, token, symbol, lexer);
		}
		accept(ident);
		pn->forbiddenchar(lexer, symbol, followers);
	}
}

void Parser::varpart(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->st_varpart, 18);
	if (symbol->symbol == varsy)
	{
		accept(varsy);
		do
		{
			vardefinition(pn->set_disjunct(pn->set_disjunct(pn->after_var, followers), pn->id_starters));
			accept(semicolon);
		} while (!pn->belong(symbol->symbol, followers));
		pn->forbiddenchar(lexer, symbol, followers);
	}
}

void Parser::vardefinition(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	vector<CIdent *> listvar;
	vector<string> names;
	vector<textposition> tokens;
	pn->skip_if_bad(lexer, symbol, followers, pn->id_starters, 2);
	if (symbol->symbol == ident)
	{
		listvar.push_back(new CIdent(CIdent::VARS));
		names.push_back(symbol->name);
		tokens.push_back(symbol->token);
		accept(ident);
		while (symbol->symbol == comma)
		{
			accept(comma);
			if (symbol->symbol == ident)
			{
				listvar.push_back(new CIdent(CIdent::VARS));
				names.push_back(symbol->name);
				tokens.push_back(symbol->token);
			}
			accept(ident);
		}
		if (symbol->symbol != colon) pn->skipto(pn->id_starters, followers, lexer, symbol);
		accept(colon);
		if (symbol->symbol == ident)
		{
			sem->addvars(listvar, names, tokens, symbol, lexer);
		}
		accept(ident);
		pn->forbiddenchar(lexer, symbol, followers);
	}
}

void Parser::procpart(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->st_procfuncpart, 18);
	while (symbol->symbol == proceduresy)
	{
		procstatement(pn->after_var);
		accept(semicolon);
	}
	pn->forbiddenchar(lexer, symbol, followers);
}

void Parser::procstatement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	auto proc = procheader(pn->set_disjunct(pn->begpart, pn->id_starters));
	//checkforward 		del scope
	if (symbol->symbol == ident && symbol->name == "forward")
	{
		proc->forward = true;
		sem->deletescope();
		accept(ident);
	}
	else
	{
		block(pn->after_var);
		sem->deletescope();
		pn->forbiddenchar(lexer, symbol, followers);
	}
}

CProcedure * Parser::procheader(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->proc_set, 18);
	accept(proceduresy);
	auto id = new CProcedure();
	if (sem->getidentfromthisscope(symbol->name) != nullptr)
	{
		try 
		{
			CProcedure* maybeforward = (CProcedure*)sem->getidentfromthisscope(symbol->name);

			if (maybeforward->forward)
			{
				sem->createscope();
				id = maybeforward;
			}
			else
			{
				lexer->error(101);
				delete id;
				id = nullptr;
			}
			maybeforward = nullptr;
		}
		catch (...){}
	}
	else
	{
		sem->addproc(id, symbol->name);
		sem->createscope();
	}
	accept(ident);
	if (symbol->symbol == leftpar)
	{
		if (id->forward) lexer->error(119);
		accept(leftpar);
		parameters(pn->set_disjunct(pn->par_set, followers), id);
		while (symbol->symbol == semicolon)
		{
			accept(semicolon);
			parameters(pn->set_disjunct(pn->par_set, followers), id);
		}
		accept(rightpar);
	}
	accept(semicolon);
	if (id!=nullptr) id->forward = false;
	pn->forbiddenchar(lexer, symbol, followers);
	return id;
}

void Parser::parameters(bitset<ParserNeutralization::SIZE_SYMBOLS> followers, CProcedure *proc)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->st_params, 2);
	switch (symbol->symbol)
	{
	case varsy: 
		accept(varsy);
		groupparameters(pn->set_disjunct(pn->after_var, followers), proc, 0); 
		break;
	default: groupparameters(pn->set_disjunct(pn->after_var, followers), proc, 1); break;
	}
	pn->forbiddenchar(lexer, symbol, followers);
}

void Parser::groupparameters(bitset<ParserNeutralization::SIZE_SYMBOLS> followers, CProcedure *proc, int metftype)
{
	//pn->skip_if_bad(lexer, symbol, followers, pn->id_starters, 2);
	//accept(ident);
	//while (symbol->symbol == comma)
	//{
	//	accept(comma);
	//	accept(ident);
	//}
	//accept(colon);
	//accept(ident);
	//pn->forbiddenchar(lexer, symbol, followers);
	vector<CIdent *> listvar;
	vector<string> names;
	vector<textposition> tokens;
	pn->skip_if_bad(lexer, symbol, followers, pn->id_starters, 2);
	if (symbol->symbol == ident)
	{
		listvar.push_back(new CIdent(CIdent::VARS));
		names.push_back(symbol->name);
		tokens.push_back(symbol->token);
		accept(ident);
		while (symbol->symbol == comma)
		{
			accept(comma);
			if (symbol->symbol == ident)
			{
				listvar.push_back(new CIdent(CIdent::VARS));
				names.push_back(symbol->name);
				tokens.push_back(symbol->token);
			}
			accept(ident);
		}
		accept(colon);
		if (symbol->symbol == ident)
		{
			sem->addparam(listvar, names, tokens, symbol, lexer, proc, metftype);
		}
		accept(ident);
		pn->forbiddenchar(lexer, symbol, followers);
	}
}

void Parser::statementpart()
{
	compositeoperator(pn->set_disjunct(pn->oper, pn->f_expr));
}

void Parser::compositeoperator(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->begin_set, 17);
	accept(beginsy);
	statement(followers);
	if (symbol->symbol == semicolon || symbol->symbol != semicolon && symbol->symbol != endsy) {
		while (symbol->symbol != 0 && symbol->symbol != endsy && symbol->symbol!=point)
		{
			accept(semicolon);
			statement(followers);
		}
	}
	accept(endsy);
	pn->forbiddenchar(lexer, symbol, followers);
}

void Parser::statement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	if (symbol->symbol == endsy || symbol->symbol == 0) return;
	//pn->skip_if_bad(lexer, symbol, followers, pn->st_stat, 17);
	CIdent * id;
	switch (symbol->symbol)
	{
	case ident:
		id = sem->getidentfromname(symbol->name);
		if (id == nullptr)
		{
			lexer->error(104);
			sem->neutralization(CIdent::VARS, symbol->name);
			accept(ident);
			pn->skipto(pn->after_var, lexer, symbol);
		}
		else
			if (id->useclass == CIdent::VARS)
				assignment(followers);
			else
				procedurestatement(followers);
		id = nullptr;
		break;
	case beginsy:
		compositeoperator(followers); break;
	case ifsy:
		ifstatement(followers); break;
	case whilesy:
		whilestatement(followers); break;
	case casesy:
		casestatement(followers); break;
	default: break;
	}
	pn->forbiddenchar(lexer, symbol, followers);
}

void Parser::procedurestatement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->id_starters, 2);
	CProcedure* proc = (CProcedure*)sem->getidentfromname(symbol->name);
	vector<Type*> types;
	vector<textposition> tokens;
	accept(ident);
	if (symbol->symbol == leftpar)
	{
		accept(leftpar);
		tokens.push_back(symbol->token);
		types.push_back(actualparameter(pn->set_disjunct(pn->parameter_set, followers)));
		while (symbol->symbol == comma)
		{
			accept(comma);
			tokens.push_back(symbol->token);
			types.push_back(actualparameter(pn->set_disjunct(pn->parameter_set, followers)));
		}
		accept(rightpar);
	}
	sem->checkprocparams(proc, types, tokens, lexer);
	pn->forbiddenchar(lexer, symbol, followers);
	proc = nullptr;
}

Type * Parser::actualparameter(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->set_disjunct(pn->id_starters, pn->st_expr), 2);
	Type *exprtype;
	exprtype = expression(followers);
	pn->forbiddenchar(lexer, symbol, followers);
	return exprtype;
}

void Parser::assignment(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->id_starters, 2);
	Type* vartype = nullptr;
	if (symbol->symbol == ident) 
	{
		auto var = sem->getidentfromname(symbol->name);
		if (var == nullptr)
		{
			lexer->error(104); 
			sem->neutralization(CIdent::VARS, symbol->name);
		}
		else
		{
			vartype = var->identtype;
		}
		var = nullptr;
	}
	accept(ident);
	if (symbol->symbol != assign) pn->skipto(pn->st_expr, lexer, symbol);
	accept(assign);
	auto exprtype = expression(followers);
	if (!sem->compatible_assign(vartype, exprtype))
		lexer->error(145);
	pn->forbiddenchar(lexer, symbol, followers);
}

Type* Parser::expression(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->st_expr, 310);
	unsigned operation;
	auto ex1type = simpleexpr(pn->set_disjunct(followers, pn->op_rel));
	if (pn->belong(symbol->symbol, pn->op_rel))
	{
		operation = symbol->symbol; symbol = lexer->nextsym();
		auto ex2type = simpleexpr(followers);
		ex1type = sem->comparing(ex1type, ex2type, operation, lexer);
	}
	pn->forbiddenchar(lexer, symbol, followers);
	return ex1type;
}

Type* Parser::simpleexpr(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	bool sign = false;
	unsigned operation;
	if (symbol->symbol == symbols::plus || symbol->symbol == symbols::minus)
	{
		lexer->nextsym();
		sign = true;
	}
	auto ex1type = term(pn->set_disjunct(followers, pn->op_add));
	if (sign) sem->right_sign(ex1type, lexer);
	while (pn->belong(symbol->symbol, pn->op_add))
	{
		operation = symbol->symbol; symbol = lexer->nextsym();
		auto ex2type = term(pn->set_disjunct(followers, pn->op_add));
		ex1type = sem->test_add(ex1type, ex2type, operation, lexer);
	}
	pn->forbiddenchar(lexer, symbol, followers);
	return ex1type;
}

Type* Parser::term(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	auto ex1type = factor(pn->set_disjunct(pn->op_mult, followers));
	while (pn->belong(symbol->symbol, pn->op_mult))
	{
		auto operation = symbol->symbol; symbol = lexer->nextsym();
		auto ex2type = factor(pn->set_disjunct(pn->op_mult, followers));
		ex1type = sem->test_mult(ex1type, ex2type, operation, lexer);
	}
	pn->forbiddenchar(lexer, symbol, followers);
	return ex1type;
}

Type* Parser::factor(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	Type *exptype = nullptr;
	CIdent* id;
	switch (symbol->symbol)
	{
	case ident: 
		id = sem->getidentfromname(symbol->name);
		if (id != nullptr)
		{
			switch (id->useclass)
			{
			case CIdent::CONSTS:
			case CIdent::VARS:
				exptype = id->identtype;
				break;
			default: exptype = nullptr;
			}
		}
		else
		{
			sem->neutralization(CIdent::VARS, symbol->name);
			lexer->error(104);
			exptype = nullptr;
		}
		accept(ident);
		break;
	case intc: accept(intc); exptype = sem->inttype; break;
	case floatc: accept(floatc); exptype = sem->realtype; break;
	case charc: accept(charc); exptype = sem->chartype; break;
	case stringc: accept(stringc); exptype = sem->stringtype; break;
	case leftpar: accept(leftpar); exptype = expression(pn->set_disjunct(pn->par_set, followers)); accept(rightpar); break;
	case notsy: accept(notsy); exptype = factor(followers); exptype = sem->checklogical(exptype, lexer); break;
	}
	pn->forbiddenchar(lexer, symbol, followers);
	return exptype;
}

void Parser::ifstatement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	accept(ifsy); 
	auto exptype = expression(pn->set_disjunct(pn->then_set, followers));
	exptype = sem->checklogical(exptype, lexer);
	if (symbol->symbol != thensy) pn->skipto(pn->oper, lexer, symbol);
	accept(thensy); 
	if (pn->belong(symbol->symbol, pn->st_stat)) statement(pn->set_disjunct(pn->else_set, followers));
	if (symbol->symbol == elsesy)
	{
		accept(elsesy);
		if (pn->belong(symbol->symbol, pn->st_stat)) statement(followers);
	}
	pn->forbiddenchar(lexer, symbol, followers);
}

void Parser::casestatement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	set<double> *labels = new set<double>();
	accept(casesy);
	auto exprtype = expression(followers);
	exprtype = sem->checkcase(exprtype, lexer);
	accept(ofsy);
	elemlistvar(pn->const_set, exprtype, labels);
	while (symbol->symbol == semicolon)
	{
		accept(semicolon);
		elemlistvar(followers, exprtype, labels);
	}
	accept(endsy);
	delete labels;
	labels = nullptr;
	pn->forbiddenchar(lexer, symbol, followers);
}

void Parser::elemlistvar(bitset<ParserNeutralization::SIZE_SYMBOLS> followers, Type* type, set<double> *labels)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->st_label, 311);
	if (symbol->symbol == symbols::plus || symbol->symbol == symbols::minus || symbol->symbol == intc || symbol->symbol == charc)
	{
		listlabels(pn->set_disjunct(followers, pn->colon_set), labels, type);
		accept(colon);
		statement(followers);
	}
	pn->forbiddenchar(lexer, symbol, followers);
}

void Parser::listlabels(bitset<ParserNeutralization::SIZE_SYMBOLS> followers, set<double> *labels, Type* type)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->st_label, 311);
	constant(pn->set_disjunct(followers, pn->colon_set), labels, type);
	while (symbol->symbol == comma)
	{
		accept(comma);
		constant(followers, labels, type);
	}
	pn->forbiddenchar(lexer, symbol, followers);
}


void Parser::constant(bitset<ParserNeutralization::SIZE_SYMBOLS> followers, set<double> *labels, Type* type)
{
	pn->skip_if_bad(lexer, symbol, followers, pn->st_label, 311);
	switch (symbol->symbol)
	{
	case symbols::plus: 
		accept(symbols::plus); 
		if (symbol->symbol == intc)
		{
			checkconstant(type, sem->inttype, symbol->nmb_int, labels);
		}
		accept(intc); 
		break;
	case symbols::minus: 
		accept(symbols::minus); 
		if (symbol->symbol == intc)
		{
			checkconstant(type, sem->inttype, -symbol->nmb_int, labels);
		}
		accept(intc); 
		break;
	case intc: 
		if (symbol->symbol == intc)
		{
			checkconstant(type, sem->inttype, symbol->nmb_int, labels);
		}
		accept(intc); 
		break;
	case charc: 
		if (symbol->symbol == charc)
		{
			checkconstant(type, sem->chartype, symbol->one_symbol, labels);
		}
		accept(charc); 
		break;
	}
	//pn->forbiddenchar(lexer, symbol, followers);
}

void Parser::checkconstant(Type * type1, Type * type2, double elem, set<double> *labels)
{
	if (type1 == nullptr || type2 == nullptr) return;
	if (type1 != type2)
		lexer->error(147);
	else
	{
		if (labels->find(elem) == labels->end())
			labels->insert(elem);
		else
			lexer->error(156);
	}
}

void Parser::whilestatement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers)
{
	accept(whilesy);
	auto exprtype = expression(followers);
	exprtype = sem->checklogical(exprtype, lexer);
	if (symbol->symbol != dosy) pn->skipto(pn->oper, lexer, symbol);
	accept(dosy);
	statement(followers);
	pn->forbiddenchar(lexer, symbol, followers);
}