#pragma once
#include <bitset>
#include "Lexer.h"
using namespace std;

class ParserNeutralization
{
public:
	static const size_t SIZE_SYMBOLS = 100;
	ParserNeutralization();
	~ParserNeutralization();
	bitset<SIZE_SYMBOLS> begpart, st_varpart, st_procfuncpart, id_starters, after_var, start, program_set, proc_set, par_set, st_params, begin_set, oper,
		else_set, then_set, op_mult, op_add, op_rel, assign_set, colon_set, st_label, st_expr, st_stat, f_expr, parameter_set, const_set;
	bool belong(unsigned element, bitset<SIZE_SYMBOLS> set);
	bitset<SIZE_SYMBOLS> set_disjunct(bitset<SIZE_SYMBOLS> set1, bitset<SIZE_SYMBOLS> set2);
	void skipto(bitset<SIZE_SYMBOLS> set, CLexer *lex, CSymbol *sym);
	void skipto(bitset<SIZE_SYMBOLS> se1, bitset<SIZE_SYMBOLS> set2, CLexer *lex, CSymbol *sym);
	void forbiddenchar(CLexer *lex, CSymbol *sym, bitset<SIZE_SYMBOLS> followers);
	void skip_if_bad(CLexer *lex, CSymbol *sym, bitset<SIZE_SYMBOLS> followers, bitset<SIZE_SYMBOLS> starters, unsigned error);
};

