#pragma once
#include <vector>
#include <map>
#include "Type.h"
using namespace std;

class CIdent
{
public:
	enum UseClass { PROGS, CONSTS, TYPES, VARS, PROCS };
	explicit CIdent(UseClass clas);
	~CIdent();
	UseClass useclass;
	Type *identtype;
};

class ProcParam
{
public:
	ProcParam(int mettransf);
	~ProcParam();

	Type *paramtype;
	int mettransf; //������ ��������
};

class CProcedure: public CIdent
{
public:
	CProcedure();
	~CProcedure();

	vector<ProcParam*> parameters;
	bool forward; //����������� ��������
};

class CConst : public CIdent
{
public:
	CConst(int intval);
	~CConst();

	int intval;
};

class Scope
{
public:
	Scope(Scope *localscope);
	~Scope();
	
	map<string, CIdent*> identtable;
	vector<Type*> typetable;
	Scope* enclosingscope; //���������� ???
};
