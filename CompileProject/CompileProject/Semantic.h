#pragma once
#include "Ident.h"
#include "CSymbol.h"
#include "Lexer.h"
class Semantic
{
public:
	Semantic();
	~Semantic();

	Scope *localscope;
	const int MAXINT = 32738;
	Type *realtype, *chartype, *booltype, *inttype, *stringtype;

	void createfictscope();
	void deletefictscope();
	void createscope();
	void deletescope();
	Type* gettypefromident(CSymbol *symbol, CLexer* lexer);
	void addvars(vector<CIdent*> idents, vector<string> names, vector<textposition> tokens, CSymbol* type, CLexer* lexer);
	void addprog(CSymbol *progident);
	void addtype(string name, textposition token, CSymbol *type, CLexer* lexer);
	void addproc(CIdent *ident, string name);
	void addparam(CIdent *id, string name);
	void addparam(vector<CIdent*> idents, vector<string> names, vector<textposition> tokens, CSymbol* type, CLexer* lexer, CProcedure *proc, int mettransf);
	CIdent* getidentfromname(string name);
	CIdent* getidentfromthisscope(string name);
	Type *checklogical(Type * type, CLexer *lexer);
	Type* test_mult(Type* type1, Type* type2, unsigned op, CLexer* lexer);
	Type* test_add(Type* type1, Type* type2, unsigned op, CLexer* lexer);
	void right_sign(Type* type, CLexer * lexer);
	Type* comparing(Type* type1, Type* type2, unsigned op, CLexer* lexer);
	bool compatible_assign(Type* vartype, Type* exprtype);
	Type* checkcase(Type* exprtype, CLexer * lexer);
	void neutralization(CIdent::UseClass cl, string name);
	void checkprocparams(CProcedure * proc, vector<Type*> types, vector<textposition> tokens, CLexer* lexer);
};

