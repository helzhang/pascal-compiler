#include "CSymbol.h"
#include "Lexer.h"
#include "ParserNeutralization.h"
#include "Semantic.h"
#include <set>
#pragma once
class Parser
{
public:
	Parser(string ifile, string ofile, bool isAppend);
	~Parser();
	CSymbol *symbol;
	CLexer *lexer;
	Semantic *sem;
	
private:
	ParserNeutralization *pn;
	void accept(unsigned symbolexpected);
	void block(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void typepart(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void varpart(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void procpart(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void statementpart();
	void typedefinition(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void vardefinition(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void procstatement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	CProcedure * procheader(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void parameters(bitset<ParserNeutralization::SIZE_SYMBOLS> followers, CProcedure *proc);
	void groupparameters(bitset<ParserNeutralization::SIZE_SYMBOLS> followers, CProcedure *proc, int metftype);
	void statement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void compositeoperator(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void complicatedoperator();
	void ifstatement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void casestatement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void whilestatement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	Type* expression(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void elemlistvar(bitset<ParserNeutralization::SIZE_SYMBOLS> followers, Type* type, set<double> *labels);
	void listlabels(bitset<ParserNeutralization::SIZE_SYMBOLS> followers, set<double> *labels, Type* type);
	void constant(bitset<ParserNeutralization::SIZE_SYMBOLS> followers, set<double> *labels, Type* type);
	void checkconstant(Type* type1, Type* type2, double elem, set<double> *labels);
	void assignment(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	Type* simpleexpr(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	Type* term(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	Type* factor(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void procedurestatement(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	Type * actualparameter(bitset<ParserNeutralization::SIZE_SYMBOLS> followers);
	void createparams(CProcedure *proc, int mettransf, bool isProc, Type* paramtype);
public:
	void programm();
};

