#pragma once
#include <vector>
//#include "Ident.h"
using namespace std;

class Type
{
public:
	enum ClassType { SCALARS, ENUMS };
	Type(ClassType type);
	~Type();
	ClassType nametype;
};

class ScalarType :public Type
{
public:
	ScalarType();
	~ScalarType();
};

class EnumType :public Type
{
public:
	EnumType();
	~EnumType();
	vector<string> constlist;
};


