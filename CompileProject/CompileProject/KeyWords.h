#pragma once
#include "SymConsts.h"
struct key
{
	unsigned codekey;
	string namekey;
} keywords[] =
{
	{ ident, " " },
	{ dosy, "do" },
	{ ifsy, "if" },
	{ insy, "in" },
	{ ofsy, "of" },
	{ orsy, "or" },
	{ tosy, "to" },
	{ ident, " " },
	{ andsy, "and" },
	{ divsy, "div" },
	{ endsy, "end"},
	{ forsy,  "for"},
	{ modsy, "mod" },
	{ nilsy, "nil" },
	{ notsy, "not" },
	{ setsy, "set" },
	{ varsy, "var" },
	{ ident, " " },
	{ casesy, "case" },
	{ elsesy, "else" },
	{ filesy, "file" },
	{ gotosy, "goto" },
	{ thensy, "then" },
	{ typesy, "type" },
	{ withsy, "with" },
	{ ident, " " },
	{ arraysy, "array" },
	{ beginsy, "begin" },
	{ constsy, "const" },
	{ labelsy, "label" },
	{ untilsy, "until" },
	{ whilesy, "while" },
	{ ident, " " },
	{ downtosy, "downto" },
	{ packedsy, "packed" },
	{ recordsy, "record" },
	{ repeatsy, "repeat" },
	{ ident, " " },
	{ programsy, "program" },
	{ ident, " " },
	{ functionsy, "function" },
	{ ident, " " },
    { proceduresy, "procedure" },
    { ident, " " }
};

short last[] = { -1, 0, 7, 17, 25, 32, 37, 39, 41, 43 };
