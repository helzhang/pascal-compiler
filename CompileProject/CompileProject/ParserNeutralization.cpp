#include "ParserNeutralization.h"



ParserNeutralization::ParserNeutralization()
{
	begpart.reset();
	begpart[34] = true; begpart[30] = true; begpart[80] = true; begpart[17] = true;
	st_varpart.reset();
	st_varpart[30] = true; st_varpart[80] = true; st_varpart[17] = true;
	st_procfuncpart.reset();
	st_procfuncpart[80] = true; st_procfuncpart[17] = true;
	id_starters.reset();
	id_starters[2] = true;
	after_var.reset();
	after_var[14] = true;
	start.reset();
	start[61] = true;
	program_set.reset();
	program_set[3] = true; /*program*/
	proc_set.reset();
	proc_set[80] = true; /*procedure*/
	par_set.reset();
	par_set[4] = true; /*rightpar*/
	st_params.reset();
	st_params[2] = true; /*ident*/ st_params[80] = true; /*procedure*/ st_params[30] = true; /*var*/
	begin_set.reset();
	begin_set[17] = true; /*begin*/
	oper.reset();
	oper[61] = true; /*point*/ oper[14] = true; /*;*/ oper[56] = true; /*if*/ oper[41] = true; /*while*/ oper[31] = true; /*case*/ oper[2] = true; oper[17] = true;/*begin*/ oper[13] = true; /*end*/
	else_set.reset();
	else_set[32] = true; /*else*/
	then_set.reset();
	then_set[52] = true; /*then*/
	op_mult.reset();
	op_mult[21] = true;/*star*/ op_mult[60] = true; /*/*/ op_mult[25] = true; /*div*/ op_mult[27] = true; /*mod*/ op_mult[24] = true; /*and*/
	op_add.reset();
	op_add[70] = true; /*plus*/ op_add[71] = true; /*minus*/ op_add[23] = true; /*or*/
	op_rel.reset();
	op_rel[16] = true; /*=*/ op_rel[65] = true; op_rel[66] = true; op_rel[67] = true; op_rel[68] = true; op_rel[69] = true;
	assign_set.reset();
	assign_set[51] = true;
	colon_set.reset();
	colon_set[5] = true;
	st_expr.reset();
	st_expr[2] = true; /*ident*/ st_expr[15] = true; /*int*/ st_expr[82] = true; /*float*/ st_expr[83] = true; /*char*/ st_expr[85] = true; /*string*/
	st_expr[9] = true; /*(*/ st_expr[29] = true; /*not*/ st_expr[70] = true; /*+*/ st_expr[71] = true; /*-*/
	st_label.reset();
	st_label[15] = true; /*int*/ st_label[83] = true; /*char*/ st_label[70] = true; /*+*/ st_label[71] = true; /*-*/
	st_stat.reset();
	st_stat[2] = true; /*ident*/ st_stat[17] = true; /*begin*/ st_stat[56] = true; /*if*/ st_stat[41] = true; /*while*/ st_stat[31] = true; /*case*/
	f_expr.reset();
	f_expr[54] = true; f_expr[52] = true; f_expr[8] = true;
	parameter_set.reset();
	parameter_set[4] = true; /*rightpar*/ parameter_set[20] = true;
	const_set.reset();
	const_set[13] = true; const_set[14] = true; 
}


ParserNeutralization::~ParserNeutralization()
{
}

bool ParserNeutralization::belong(unsigned element, bitset<SIZE_SYMBOLS> set)
{
	return set[element];
}

bitset<ParserNeutralization::SIZE_SYMBOLS> ParserNeutralization::set_disjunct(bitset<SIZE_SYMBOLS> set1, bitset<SIZE_SYMBOLS> set2)
{
	return set1 | set2;
}

void ParserNeutralization::skipto(bitset<SIZE_SYMBOLS> set, CLexer *lex, CSymbol *sym)
{
	while (!belong(sym->symbol, set) && sym->symbol != 0)
	{
		sym = lex->nextsym();
	}
}

void ParserNeutralization::skipto(bitset<SIZE_SYMBOLS> set1, bitset<SIZE_SYMBOLS> set2, CLexer *lex, CSymbol *sym)
{
	while (!belong(sym->symbol, set1) && !belong(sym->symbol, set2) && sym->symbol != 0)
	{
		sym = lex->nextsym();
	}
}

void ParserNeutralization::forbiddenchar(CLexer * lex, CSymbol *sym, bitset<SIZE_SYMBOLS> followers)
{
	if (!belong(sym->symbol, followers))
	{
		lex->error(6);
		skipto(followers, lex, sym);
	}
}

void ParserNeutralization::skip_if_bad(CLexer *lexer, CSymbol *symbol, bitset<SIZE_SYMBOLS> followers, bitset<SIZE_SYMBOLS> starters, unsigned error)
{
	if (!belong(symbol->symbol, starters))
	{
		lexer->error(error);
		skipto(starters, followers, lexer, symbol);
	}
}

