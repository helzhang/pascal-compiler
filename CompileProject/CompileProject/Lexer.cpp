#include <cctype>
#include "Lexer.h"
#include "ErrorConstList.h"
#include "KeyWords.h"

CLexer::CLexer(string ifile, string ofile, bool isApp)
{
	if (isApp)
	{
		ofstr.open(ofile, ios::app);
	}
	else
	{
		ofstr.open(ofile);
	}
	ifstr.open(ifile);
	positionnow.charnumber = 1;
	positionnow.linenumber = 1;
	getline(ifstr, line); line += '\n';
	ch = line[0];
	symbol = new CSymbol();
}



CLexer::~CLexer()
{
	ofstr.close();
	ifstr.close();
	delete symbol;
	symbol = nullptr;
}

void CLexer::nextch()
{
	if (ch == '\n')
	{
		ofstr << positionnow.linenumber << "      " << line;
		unsigned c = 1, num = positionnow.linenumber; while (num /= 10) c++;
		auto v = FindErrors(positionnow.linenumber);
		if (v.size() != 0)
			PrintErrors(v, c + 6);
		if (getline(ifstr, line)) {
			line += '\n';
			positionnow.linenumber++;
			positionnow.charnumber = 1;
		}
		else
		{
			ch = EOF;
			return;
		}
	}
	else positionnow.charnumber++;
	ch = line[positionnow.charnumber-1];
}

void CLexer::error(unsigned errorcode)
{
	if (ErrList.size() != ERRMAX)
	{
		errorList el;
		el.errorcode = errorcode;
		el.errorposition = /*positionnow*/symbol->token;
		ErrList.push_back(el);
	}
}

void CLexer::error(unsigned errorcode, textposition position)
{
	if (ErrList.size() != ERRMAX)
	{
		errorList el;
		el.errorcode = errorcode;
		el.errorposition = position;
		ErrList.push_back(el);
	}
}

vector<errorList> CLexer::FindErrors(unsigned linenum)
{
	vector<errorList> v;
	for (auto i = ErrList.begin(); i != ErrList.end(); i++)
	{
		if ((*i).errorposition.linenumber == linenum)
			v.push_back(*i);
	}
	return v;
}

void CLexer::PrintErrors(vector<errorList> errline, unsigned num)
{
	for (auto i = errline.begin(); i != errline.end(); i++)
	{
		errorcount++;
		short c = 1, n = errorcount; while (n /= 10) c++;
		ofstr << "**" << errorcount << "**";
		c += 4;
		for (unsigned j = c; j < num+(*i).errorposition.charnumber-1; j++) ofstr << ' ';
		ofstr << "^ ������ ��� " << (*i).errorcode << endl <<"*****   " << errorconstlist[(*i).errorcode]<< endl;
	}
}

void CLexer::printError(unsigned code, textposition position)
{
	errorcount++;
	short c = 1, n = errorcount; while (n /= 10) c++;
	ofstr << "**" << errorcount << "**";
	c += 4;
	unsigned d = 1, num = position.linenumber; while (num /= 10) d++;
	for (unsigned j = c; j < d + 6 + position.charnumber - 1; j++) ofstr << ' ';
	ofstr << "^ ������ ��� " << code << endl << "*****   " << errorconstlist[code] << endl;
}

CSymbol* CLexer::nextsym()
{
	if (ch == EOF) 
	{ symbol->symbol = 0;  return symbol; }
	while (ch == ' ' || ch == '\t') nextch();
	symbol->token.linenumber = positionnow.linenumber;
	symbol->token.charnumber = positionnow.charnumber;
		if (ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z' || ch=='_')
		{
			Words();
		}
		else if (ch >= '0' && ch <= '9')
		{
			NumConst();
		}
		else
			switch (ch)
			{
			case '\n': nextch(); nextsym(); break;
			case '{': MultiLineComm(false); break;
			case '(': nextch(); if (ch == '*') MultiLineComm(true); else symbol->symbol = leftpar; break;
			case ')': symbol->symbol = rightpar; nextch(); break;
			case '*': symbol->symbol = star; nextch(); break;
			case '/': nextch();
				if (ch == '/') {
					OneLineComm();
				}
				else  symbol->symbol = slash; break;
			
			case '=': 
				symbol->symbol = symbols::equal; nextch();
				 break;
			case ',': symbol->symbol = comma; nextch(); break;
			case ';': symbol->symbol = semicolon; nextch(); break;
			case '^': symbol->symbol = arrow; nextch(); break;
			case '[': symbol->symbol = lbracket; nextch(); break;
			case ']': symbol->symbol = rbracket; nextch(); break;
			case '+': symbol->symbol = symbols::plus; nextch(); break;
			case '-':symbol->symbol = symbols::minus; nextch(); break;
			case '>': 
				nextch();
				if (ch == '=')
				{
					symbol->symbol = greaterequal; nextch();
				}
				else
					symbol->symbol = greater;
				break;
			default: error(6); nextch(); nextsym(); break;
			case ':': nextch();
				if (ch == '=') {
					symbol->symbol = assign; nextch();
				}
				else  symbol->symbol = colon; break;
			case '.': nextch();
				if (ch == '.') {
					symbol->symbol = twopoints; nextch();
				}
				else  symbol->symbol = point; break;
			case '<': nextch();
				if (ch == '=')
				{
					symbol->symbol = laterequal; nextch();
				}
				else
					if (ch == '>')
					{
						symbol->symbol = latergreater; nextch();
					}
					else
						symbol->symbol = later;
				break;
			case '\'': nextch(); 
				SymbolConst();
					break;
			}
	return symbol;
}

void CLexer::MultiLineComm(bool isParstar)
{
	if (!isParstar) {
		do nextch();
		while (ch != EOF && ch != '}');
	}
	else
	{
		while (ch != EOF) {
			do nextch();
			while (ch != EOF && ch != '*');
			if (ch != EOF) nextch();
			if (ch == ')') break;
		}
	}
	if (ch == EOF) {
		error(86, positionnow);
		unsigned c = 1, num = positionnow.linenumber; while (num /= 10) c++;
		auto v = FindErrors(positionnow.linenumber);
		if (v.size() != 0)
			PrintErrors(v, c + 6);
		symbol->symbol = 0;
	}
	else {
		nextch();
		nextsym();
	}
}

void CLexer::OneLineComm()
{
	while (ch != '\n') nextch();
	//vector<errorList> v = FindErrors(positionnow.linenumber);
	//unsigned c = 1, num = positionnow.linenumber; while (num /= 10) c++;
	//if (v.size() != 0)
	//	PrintErrors(v, c + 6);
	nextch(); 
	nextsym();
}

void CLexer::SymErr()
{
	textposition position;
	position.charnumber = positionnow.charnumber - 1;
	position.linenumber = positionnow.linenumber;
	error(75, position);
	nextsym();
}

void CLexer::SymbolConst()
{
	//- �������������
	symbol->str_const = "";
	while (ch != '\n' && ch != '\'')
	{
		symbol->str_const += ch;
		nextch();
	}
	if (ch == '\n') {
		SymErr(); return;
	}
	nextch();
	if (symbol->str_const.size() == 1) { symbol->one_symbol = symbol->str_const[0]; symbol->symbol = charc; }
	else
	{
		symbol->symbol = stringc;
	}
}

void CLexer::NumConst()
{
	string s = "";
	bool isDbl = false;
	while (ch >= '0' && ch <= '9')
	{
		s += ch;
		nextch();
	}
	switch (ch)
	{
	case '.':
		isDbl = true;
		s += ch;
		nextch();
		if (ch > '9' || ch < '0')
		{
			error(201, positionnow);
			symbol->symbol = floatc;
			return;
		}
		else
		{
			while (ch >= '0' && ch <= '9')
			{
				s += ch;
				nextch();
			}
			if (ch == 'e' || ch == 'E')
			{
				s += ch;
				nextch();
				if ((ch > '9' || ch < '0') && ch != '-' && ch != '+')
				{
					error(201, positionnow);
					symbol->symbol = floatc;
					return;
				}
				else
				{
					if (ch == '+' || ch == '-')
					{
						s += ch;
						nextch();
					}
					if (ch > '9' || ch < '0')
					{
						error(201, positionnow);
						symbol->symbol = floatc;
						return;
					}
					while (ch >= '0' && ch <= '9')
					{
						s += ch;
						nextch();
					}
				}
			}
		}
		break;
	case 'e':
	case 'E':
		isDbl = true;
		s += ch;
		nextch();
		if ((ch > '9' || ch < '0') && ch != '-' && ch != '+')
		{
			error(201, positionnow);
			symbol->symbol = floatc;
			return;
		}
		else
		{
			if (ch == '+' || ch == '-')
			{
				s += ch;
				nextch();
			}
			if (ch > '9' || ch < '0')
			{
				error(201, positionnow);
				symbol->symbol = floatc;
				return;
			}
			while (ch >= '0' && ch <= '9')
			{
				s += ch;
				nextch();
			}
		}
		break;
	}

	char * end_ptr;
	if (isDbl)
	{
		symbol->nmb_dbl = strtod(s.c_str(), &end_ptr);
		if (errno == ERANGE && HUGE_VAL < 0 || symbol->nmb_dbl < 2.9e-39)
		{
			error(206); 
			symbol->symbol = floatc;
			return;
		}
		if (errno == ERANGE && HUGE_VAL > 0 || symbol->nmb_dbl > 1.7e38)
		{
			error(207); 
			symbol->symbol = floatc;
			return;
		}
		symbol->symbol = floatc;
	}
	else
	{
		try {
			symbol->nmb_int = stoi(s);
			if (symbol->nmb_int > 32738)
			{
				error(203); 
				symbol->symbol = intc;
				return;
			}
			symbol->symbol = intc;
		}
		catch (exception e)
		{
			error(203); symbol->symbol = intc;
		}
	}
}

void CLexer::Words()
{
	string name = "";
	int lname = 0;
	while (ch >= 'A' && ch <= 'Z' || ch >= 'a' && ch <= 'z' || ch == '_' || ch >= '0' && ch <= '9')
	{
		if (lname < MAX_LEN_IDENT)
		{
			name += (char)tolower(ch);
			lname++;
			nextch();
		}
		else
			nextch();
	}
	if (lname <= 9) {
		keywords[last[lname]].namekey = name;
		int i = last[lname - 1] + 1;
		while (keywords[i].namekey != name) i++;
		symbol->symbol = keywords[i].codekey;
		symbol->name = keywords[i].namekey;
	}
	else
	{
		symbol->symbol = ident;
		symbol->name = name;
	}
}